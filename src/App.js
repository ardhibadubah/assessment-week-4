import Child1 from './components/Child1';
import Questions from './components/questions';
import './App.css';

function App() {
  return (
    <div className="App">
      <h1>Assessment Week 4</h1>
      <Child1 />
      <Questions />
    </div>
  );
}

export default App;
